package ch.ost.rj.ifs.icon_converter.view;

import ch.ost.rj.ifs.icon_converter.controller.Controller;
import ch.ost.rj.ifs.icon_converter.controller.ListListener;
import ch.ost.rj.ifs.icon_converter.model.Icon;
import ch.ost.rj.ifs.icon_converter.model.IconList;
import org.apache.batik.swing.JSVGCanvas;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.net.MalformedURLException;

/**
 * Setzt eine Liste an Icons und eine kleine Vorschau zusammen.
 *
 * @author Simon Weibel
 * @since 2021-05-12
 */
public class IconsPanel extends JPanel implements ListSelectionListener, MouseListener {

    private Controller controller;
    private IconList iconList;
    private MainFrame mainFrame;

    private JList<Icon> selectableIconsList;
    private JSVGCanvas svgCanvas;

    /**
     * Konstruktor der Klasse.
     * Weist den jeweiligen Variablen MainFrame, Controller und IconList zu.
     * Erstellt ausserdem eine scrollbare Liste fuer die IconList, sowie ein JSVGCanvas, in ein Icon zur Vorschau gestellt werden kann.
     *
     * @param parent     Der MainFrame parent.
     * @param controller Der Controller.
     */
    public IconsPanel(MainFrame parent, Controller controller) {
        this.mainFrame = parent;
        this.controller = controller;
        iconList = controller.getIconList();

        var gridLayout = new GridBagLayout();
        setLayout(gridLayout);

        var gbc_scrollPanel = new GridBagConstraints();
        gbc_scrollPanel.gridx = 0;
        gbc_scrollPanel.gridy = 0;
        gbc_scrollPanel.gridheight = 2;
        gbc_scrollPanel.insets = new Insets(0, 0, 0, 10);
        gbc_scrollPanel.fill = GridBagConstraints.BOTH;
        gbc_scrollPanel.weightx = 1;
        gbc_scrollPanel.weighty = 1;

        var gbc_canvasPanel = new GridBagConstraints();
        gbc_canvasPanel.gridx = 1;
        gbc_canvasPanel.gridy = 0;
        gbc_canvasPanel.gridheight = 0;
        gbc_canvasPanel.fill = GridBagConstraints.BOTH;
        gbc_canvasPanel.insets = new Insets(0, 10, 0, 0);

        var scrollPanel = createScrollPanel();
        scrollPanel.setMinimumSize(new Dimension(200, 300));
        scrollPanel.setPreferredSize(new Dimension(200, 300));
        add(scrollPanel, gbc_scrollPanel);
        var canvasPanel = createCanvasPanel();
        canvasPanel.setMinimumSize(new Dimension(150, 300));
        canvasPanel.setPreferredSize(new Dimension(150, 300));
        add(canvasPanel, gbc_canvasPanel);

        var emptyBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        var labelBorder = BorderFactory.createTitledBorder("Icons");
        var compoundBorder = BorderFactory.createCompoundBorder(labelBorder, emptyBorder);
        setBorder(compoundBorder);
    }

    /**
     * Erstellt eine scrollbare Liste und weist ihr Listener und Liste mit Listenmodell und gibt diese dann zurueck.
     *
     * @return Eine scrollbare Liste.
     */
    private JScrollPane createScrollPanel() {
        selectableIconsList = new JList(new IconsListModel());
        selectableIconsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        selectableIconsList.getSelectionModel().addListSelectionListener(this);
        return new JScrollPane(selectableIconsList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    }

    /**
     * Erstellt ein CanvasPanel, in dem Icons zur Vorschau gestellt werden koennen.
     *
     * @return Ein JPanel, welches eine JSVGCanvas-Komponente von Batik enthaelt, welche SVGs darstellen laesst.
     */
    public JPanel createCanvasPanel() {
        var canvasPanel = new JPanel();
        canvasPanel.setLayout(new GridLayout(2, 0));

        svgCanvas = new JSVGCanvas();
        svgCanvas.setDisableInteractions(true);
        svgCanvas.addMouseListener(this);

        canvasPanel.add(svgCanvas);

        return canvasPanel;
    }

    /**
     * Setzt die URL vom svgCanvas auf das ausgewaehlte Icon.
     *
     * @param icon Das anzuzeigende Icon.
     */
    public void paintCanvas(Icon icon) {
        var file = new File(icon.getPath().toString());
        String url = null;
        try {
            url = file.toURI().toURL().toString();
        } catch (MalformedURLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        svgCanvas.setURI(url);
    }

    /**
     * Liefert die IconListe.
     *
     * @return Die IconListe.
     */
    public JList<Icon> getSelectableIconsList() {
        return selectableIconsList;
    }

    /**
     * Methode welche aufgerufen wird, wenn ein Element in der Liste angeclickt wurde.
     *
     * @param e Das ListSelectionEvent vom Selektieren eines Elements in der Liste.
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        int element = selectableIconsList.getSelectedIndex();
        if (element >= 0) {
            paintCanvas(iconList.getIcons().get(element));
        }
    }

    /**
     * Wird aufgerufen, wenn ein Mausklick getaetigt wird.
     * Mit der Robot Klasse wird dann die Farbe vom Pixel, auf den man geklickt hat gespeichert und in das Hexardezimal Format geaendert.
     *
     * @param e Das MouseEvent vom Klicken der Maus.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        Color color;
        try {
            var robot = new Robot();
            color = robot.getPixelColor(e.getXOnScreen(), e.getYOnScreen());
            String hexColor = (Integer.toHexString(color.getRGB()).substring(2).toUpperCase());
            if (!hexColor.equals("FFFFFF")) {
                mainFrame.getControlPanel().setSourceColor(hexColor);
            }
        } catch (AWTException awtE) {
            JOptionPane.showMessageDialog(null, awtE.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Leere Methode, welche aufgerugen wird, wenn man die Maus gedrueckt haelt.
     * Dies ist gewollt so, da in dieser Situation auch nichts veraendert werden soll.
     *
     * @param e Das MouseEvent vom Druecken der Maus.
     */
    @Override
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Leere Methode, welche aufgerugen wird, wenn man die Maus loslaesst.
     * Dies ist gewollt so, da in dieser Situation auch nichts veraendert werden soll.
     *
     * @param e Das MouseEvent vom Loslassen der Maus.
     */
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Leere Methode, welche aufgerugen wird, wenn man auf den SVGCanvs haelt.
     * Dies ist gewollt so, da in dieser Situation auch nichts veraendert werden soll.
     *
     * @param e Das MouseEvent vom mit der Maus auf eine Flaeche fahren.
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

    /**
     * Leere Methode, welche aufgerufen wird, wenn die Maus aus dem Canvas faehrt.
     * Dies ist gewollt so, da in dieser Situation auch nichts veraendert werden soll.
     *
     * @param e Das MouseEvent vom mit der Maus von der Flaeche gehen.
     */
    @Override
    public void mouseExited(MouseEvent e) {
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    /**
     * Eine ListenModel-Klasse fuer die Icons.
     * Sie legt das generelle Model der Liste fest.
     *
     * @author Simon Weibel
     * @since 2021-05-12
     */
    private class IconsListModel extends AbstractListModel<String> implements ListListener {

        /**
         * Konstruktor der Klasse.
         * Er fuegt das Objekt als ListListener im Controller hinzu.
         */
        public IconsListModel() {
            controller.addListListener(this);
        }

        /**
         * Gibt die Groesse der Liste zurueck.
         *
         * @return Anzahl Elemente in der IconListe.
         */
        @Override
        public int getSize() {
            return controller.getIconList().getSize();
        }

        /**
         * Gibt das Element an der Stelle index zurueck.
         *
         * @param index Index des Elements in der Liste.
         * @return Den Dateinamen des Icons.
         */
        @Override
        public String getElementAt(int index) {
            return controller.getIconList().getIcons().get(index).getFilename();
        }

        /**
         * Wird aufgerufen, wenn ein Icon hinzugefuegt wird.
         * Die Methode ruft die Methode fireIntervalAdded auf, welche die Liste aktualisiert.
         *
         * @param icon Das hinzugefuegte Icon.
         */
        @Override
        public void iconAdded(Icon icon) {
            fireIntervalAdded(this, 0, controller.getIconList().getSize());
        }
    }
}
