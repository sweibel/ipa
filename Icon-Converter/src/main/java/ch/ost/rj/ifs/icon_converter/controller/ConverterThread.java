package ch.ost.rj.ifs.icon_converter.controller;

import ch.ost.rj.ifs.icon_converter.model.Icon;
import ch.ost.rj.ifs.icon_converter.model.IconList;
import ch.ost.rj.ifs.icon_converter.view.ProgressbarDialog;
import de.vandermeer.svg2vector.applications.fh.Svg2Vector_FH;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

/**
 * Thread, welcher die Hauptfarbe aendert und gegebenfalls auch das Format aendert.
 * Der Thread laeuft neben dem GUI-Thread, somit ist die Benutzeroberflaeche waehrend dem Rechenprozess nicht festgefroren.
 *
 * @author Simon Weibel
 * @since 2021-05-17
 */
public class ConverterThread extends Thread implements ConverterListener {

    private ProgressbarDialog progressbarDialog;
    private Controller controller;
    private IconList iconList;
    private String sourceColor, targetColor, targetDirectory, targetFormat;

    /**
     * Konstruktor der Klasse.
     * Er setzt den Controller, die Fortschrittsanzeige und die IconListe.
     * Zudem setzt er auch die User-Inputs: Source Color, Target Color, Target Directory und Target Format.
     *
     * @param progressbarDialog Die Fortschrittsanzeige.
     * @param controller        Der Controller.
     */
    public ConverterThread(ProgressbarDialog progressbarDialog, Controller controller) {
        this.progressbarDialog = progressbarDialog;
        this.controller = controller;
        iconList = controller.getIconList();

        controller.addConverterListener(this);

        sourceColor = progressbarDialog.getControlPanel().getSourceColor();
        targetColor = progressbarDialog.getControlPanel().getTargetColor();
        targetDirectory = progressbarDialog.getControlPanel().getTargetDirectory();
        targetFormat = progressbarDialog.getControlPanel().getTargetFormat();
    }

    /**
     * Run Methode des Threads.
     * In Ihm findet der Konvertierungsprozess statt.
     */
    @Override
    public void run() {
        try {
            ArrayList<String> incompatibleColorIcons = new ArrayList<>();
            Path tempPath = Files.createTempDirectory("temp");
            int amountOfIcons = iconList.getSize();
            int convertedIcons = 0;

            if (targetFormat.equals("EMF")) {
                for (Icon i : iconList.getIcons()) {
                    Path originalIconPath = Paths.get(i.getPath().toString());
                    Path tempIconPath = getOutputDirectoryPath(i, tempPath);

                    String content = Files.readString(originalIconPath);
                    if (content.contains(sourceColor)) {
                        Files.copy(originalIconPath, tempIconPath, StandardCopyOption.REPLACE_EXISTING);
                        content = content.replaceAll(sourceColor, targetColor);

                        FileUtils.writeStringToFile(tempIconPath.toFile(), content, "utf-8");
                        convertFormat(tempIconPath.toString());

                        SwingUtilities.invokeLater(() -> progressbarDialog.setConvertedFileLabel(tempIconPath.getFileName().toString().replace("svg", "emf")));

                        convertedIcons++;
                        int progress = (convertedIcons * 100 / amountOfIcons * 100);
                        SwingUtilities.invokeLater(() -> progressbarDialog.getProgressBar().setValue(progress));
                    } else {
                        incompatibleColorIcons.add(i.getFilename());
                    }
                }
            } else {
                for (Icon i : iconList.getIcons()) {
                    Path original = Paths.get(i.getPath().toString());
                    Path outputPath = getOutputDirectoryPath(i, Paths.get(targetDirectory));
                    String content = Files.readString(original);
                    if (content.contains(sourceColor)) {
                        Files.copy(original, outputPath, StandardCopyOption.REPLACE_EXISTING);
                        content = content.replaceAll(sourceColor, targetColor);
                        FileUtils.writeStringToFile(outputPath.toFile(), content, "utf-8");

                        SwingUtilities.invokeLater(() -> progressbarDialog.setConvertedFileLabel(outputPath.getFileName().toString()));

                        convertedIcons++;
                        int progress = (convertedIcons * 100 / amountOfIcons);
                        SwingUtilities.invokeLater(() -> progressbarDialog.getProgressBar().setValue(progress));
                    } else {
                        incompatibleColorIcons.add(i.getFilename());
                    }
                }
            }
            if (incompatibleColorIcons.size() > 0) {
                StringBuilder message = new StringBuilder();
                for (String s : incompatibleColorIcons) {
                    message.append(System.lineSeparator()).append(s);
                }
                throw new Exception("Given source color could not be found in " + message);

            }
            deleteDir(tempPath);
            controller.finishedConversion();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(progressbarDialog, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            controller.failedConversion();
        }
    }

    /**
     * Setzt den Pfad zusammen, fuer den Ort an dem das Icon gespeichert, bzw. zwischengespeichert werden soll.
     *
     * @param i               Das zu konvertierende Icon.
     * @param outputDirectory Das Verzeichnis, in dem das Icon gespeichert/zwischengespeichert werden soll.
     * @return Den vollstaendigen Pfad fuer das zu konvertierende Icon.
     */
    private Path getOutputDirectoryPath(Icon i, Path outputDirectory) {
        String[] splitString = i.getFilename().split("\\.");
        return Paths.get(outputDirectory + "\\\\" + splitString[0] + "_" + targetColor + "." + splitString[1]);
    }

    /**
     * Loescht das angegebene Verzeichnis und seine Unterdaten.
     *
     * @param dir Pfad zum Ordner.
     * @throws IOException Falls der Ordner nicht existiert.
     */
    private void deleteDir(Path dir) throws IOException {
        File[] files = dir.toFile().listFiles();
        for (File f : files) {
            Files.delete(f.toPath());
        }
        Files.delete(dir);
    }

    /**
     * Konvertiert das Format des Files mit der Hilfe von SVG2Vector.
     *
     * @param from Der Pfadname des zu konvertierenden Files.
     */
    private void convertFormat(String from) {
        String[] converter = new String[]{
                "--create-directories", "--overwrite-existing",
                "-t", "emf",
                "-f", from.replaceAll("\\\\", "/"),
                "-d", targetDirectory.replaceAll("\\\\", "/"),
                "-w"
        };
        Svg2Vector_FH app = new Svg2Vector_FH();
        app.executeApplication(converter);
    }

    /**
     * Methode, welche den Thread killed, wenn der User in der Fortschrittsanzeige den Knopf "Abort" drueckt.
     * Die "stop()"-Methode ist zwar deprecated, jedoch fuer diese Funktion vollends geeignet.
     */
    @Override
    public void abortPressed() {
        stop();
    }
}
