package ch.ost.rj.ifs.icon_converter;

import ch.ost.rj.ifs.icon_converter.controller.Controller;
import ch.ost.rj.ifs.icon_converter.view.MainFrame;

import javax.swing.*;

/**
 * Main Klasse, die nur die main-Methode enthaelt, welche das GUI in einem GUI-Thread und den Controller startet.
 *
 * @author Simon Weibel
 * @since 20201-05-12
 */
public class IconConverterMain {

    /**
     * Main Methode, mit der das Programm mitsamt der Benutzeroberflaeche gestartet wird.
     *
     * @param args Mitgelieferte Command-Line Argumente.
     */
    public static void main(String[] args) {
        Controller controller = new Controller();
        SwingUtilities.invokeLater(() -> new MainFrame(controller));
    }

}
