package ch.ost.rj.ifs.icon_converter.controller;

/**
 * Listener, welcher auf das Druecken vom "Abort"-Knopf reagiert.
 *
 * @author Simon Weibel
 * @since 2021-05-17
 */
public interface ConverterListener {

    /**
     * Methode, welche aufgerufen wird, wenn der User den "Abort" Knopf drueckt.
     */
    void abortPressed();
}
