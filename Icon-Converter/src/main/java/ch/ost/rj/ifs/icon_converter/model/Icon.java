package ch.ost.rj.ifs.icon_converter.model;

import java.nio.file.Path;

/**
 * Repraesentiert ein Icon.
 * Es enthaelt nur den Pfadnamen und den Dateinamen als Attribute.
 *
 * @author Simon Weibel
 * @since 2021-05-21
 */
public class Icon {

    private String filename;
    private Path path;

    /**
     * Konstruktor der Klasse Icon.
     * Er weist den Parameter "path" dem neue initialisiertem Objekt als Attribut "path" zu und setzt dann auch gleich den Dateinamen.
     *
     * @param path Der Pfadname des Icons.
     */
    public Icon(Path path) {
        this.path = path;
        String[] splitPath = path.toString().split("\\\\");
        filename = splitPath[splitPath.length - 1];
    }

    /**
     * get Methode fuer den Pfadnamen.
     * Liefert den Pfadnamen.
     *
     * @return Den Pfadnamne des Icons.
     */
    public Path getPath() {
        return path;
    }

    /**
     * set Methode fuer den Pfadnamen.
     * Setzt den Pfadnamen des Objekts.
     *
     * @param path Der Pfadnamne des Icons.
     */
    public void setPath(Path path) {
        this.path = path;
    }

    /**
     * get Methode fuer den Dateinamen.
     * Liefert den Dateinamen.
     *
     * @return Der Dateiname des Icons.
     */
    public String getFilename() {
        return filename;
    }

    /**
     * set Methode fuer den Dateinamen.
     * Liefert den Dateinamen
     *
     * @param filename Der Dateiname des Icons.
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * Equals Methode, welche zwei Objekte anhand der Klasse und des paths vergleicht und true zurueck gibt, falls es dieselben Objekte sind
     *
     * @param o zu vergleichendes Objekt
     * @return Boolesches Ergebnis, ob es sich um das selbe Objekt handelt oder nicht
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Icon that = (Icon) o;
        return path.equals(that.path);
    }
}
