package ch.ost.rj.ifs.icon_converter.controller;

/**
 * Listener, welcher auf den erflogreichen Abschluss des Konvertireungsprozesses und auch das Fehlschlagen dieses reagiert.
 *
 * @author Simon Weibel
 * @since 2021-05-17
 */
public interface ProgressbarListener {

    /**
     * Methode, welche aufgerufen wird, wenn der Konvertierungsprozess erfolgreich abgeschlossen wurde.
     */
    void convertingFinished();

    /**
     * Methode, welche aufgerufen wird, wenn der Konvertierungsprozess fehlschlaegt.
     */
    void convertingFailed();
}
