package ch.ost.rj.ifs.icon_converter.view;

import ch.ost.rj.ifs.icon_converter.controller.Controller;
import ch.ost.rj.ifs.icon_converter.controller.ProgressbarListener;

import javax.swing.*;
import java.awt.*;

/**
 * Stellt den Fortschritt des Konvertierungsprozesses als Fortschrittsanzeige dar.
 *
 * @author Simon Weibel
 * @since 20201-05-12
 */
public class ProgressbarDialog extends JDialog implements ProgressbarListener {

    private Controller controller;
    private ControlPanel controlPanel;

    private JLabel convertedFileLabel;
    private JProgressBar progressBar;

    /**
     * Konstruktor vom ProgressbarDialog.
     * Er weist den ParentFrame, das ControlPanel und den Controller zu.
     * Dazu stellt er noch ein Label zusammen, was das momentan konvertierte Icon anzeigt, sowie eine Fortschrittsanzeige und und eine "Abort"-Knopf, mit dem der ganze Prozess abgebrochen werden kann.
     * Er fuegt sich selbst auch der ProgressbarListener Liste im Controller zu.
     * Bevor das Frame selbst konfiguriert wird, wird der WorkerThread ueber den Controller gestartet.
     *
     * @param parent       Der ParentFrame. Er wird benoetigt, um den Dialog modal zu gestalten.
     * @param controlPanel ueber das controlPanel werden die InputWerte eingelesen.
     * @param controller   Der Controller startet den WorkerThread und speichert das Objekt als Listener.
     */
    public ProgressbarDialog(JFrame parent, ControlPanel controlPanel, Controller controller) {
        super(parent, true);
        this.controller = controller;
        this.controlPanel = controlPanel;

        controller.addProgressbarListener(this);

        var contentPane = (JPanel) getContentPane();
        contentPane.setLayout(new GridBagLayout());

        convertedFileLabel = new JLabel("Converting...");
        var gbc_convertedFileLabel = new GridBagConstraints();
        gbc_convertedFileLabel.gridx = 0;
        gbc_convertedFileLabel.gridy = 0;
        gbc_convertedFileLabel.gridwidth = 6;
        gbc_convertedFileLabel.anchor = GridBagConstraints.WEST;
        gbc_convertedFileLabel.insets = new Insets(20, 20, 10, 20);
        contentPane.add(convertedFileLabel, gbc_convertedFileLabel);

        progressBar = new JProgressBar();
        progressBar.setPreferredSize(new Dimension(500, 20));
        var gbc_progressBar = new GridBagConstraints();
        gbc_progressBar.gridx = 0;
        gbc_progressBar.gridy = 1;
        gbc_progressBar.gridwidth = 6;
        gbc_progressBar.insets = new Insets(10, 20, 10, 20);
        contentPane.add(progressBar, gbc_progressBar);

        JButton abortButton = new JButton("Abort");
        abortButton.addActionListener((ae) -> pressedAbort());
        var gbc_abortButton = new GridBagConstraints();
        gbc_abortButton.gridx = 5;
        gbc_abortButton.gridy = 2;
        gbc_abortButton.anchor = GridBagConstraints.EAST;
        gbc_abortButton.insets = new Insets(10, 20, 20, 20);
        contentPane.add(abortButton, gbc_abortButton);

        controller.startConverterThread(this);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setLocationRelativeTo(controlPanel.getMainFrame());
        setResizable(false);
        setTitle("Convert Icons");
        pack();
        setVisible(true);

    }

    /**
     * Methode, welche den Thread darueber informiert, dass der Prozess abgeschlossen werden soll.
     * Danach wird der Dialog geschlossen.
     */
    private void pressedAbort() {
        controller.abortConversion();
        dispose();
    }

    /**
     * Liefert das ControlPanel.
     *
     * @return Das ControlPanel.
     */
    public ControlPanel getControlPanel() {
        return controlPanel;
    }

    /**
     * Liefert die Fortschrittsanzeige-Komponente.
     *
     * @return Die Fortschrittsanzeige-Komponente.
     */
    public JProgressBar getProgressBar() {
        return progressBar;
    }

    /**
     * Setzt den Text im Label, welches den Dateinamen des zuletzt konvertierenden Icons anzeigt.
     *
     * @param filename Der Name des Icons.
     */
    public void setConvertedFileLabel(String filename) {
        convertedFileLabel.setText(filename);
    }

    /**
     * Wird aufgerufen, wenn der Prozess im WorkerThread erfolgreich war.
     * Er schliesst das Fenster einfach.
     */
    @Override
    public void convertingFinished() {
        dispose();
    }

    /**
     * Wird aufgerufen, wenn der Prozess im WorkerThread fehlschlug.
     * Er schliesst das Fenster einfach.
     */
    @Override
    public void convertingFailed() {
        dispose();
    }
}
