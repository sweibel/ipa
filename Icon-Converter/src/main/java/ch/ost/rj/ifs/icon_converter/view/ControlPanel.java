package ch.ost.rj.ifs.icon_converter.view;

import ch.ost.rj.ifs.icon_converter.controller.Controller;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;

/**
 * Setzt das ControlPanel zusammen.
 * In ihm kann der User die Source Color, die Target Color, die Target Directory und das Target Format angeben.
 * Ausserdem kann er den Konvertierungsprozess starten.
 *
 * @author Simon Weibel
 * @since 2021-05-18
 */
public class ControlPanel extends JPanel {

    private Controller controller;
    private MainFrame mainFrame;

    private ArrayList<JTextField> textFields = new ArrayList<>();
    private JTextField sourceColorField, targetColorField, targetDirectoryField;
    private JComboBox targetFormatBox;
    private JButton convertButton;

    /**
     * Konstruktor der Klasse ControlPanel.
     * Er stellt die verschiedenen Labels, TextFields und Buttons zusammen.
     *
     * @param parent     Der MainFrame.
     * @param controller Der Controller.
     */
    public ControlPanel(MainFrame parent, Controller controller) {
        this.mainFrame = parent;
        this.controller = controller;

        var gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{120, 160, 90, 15, 30};
        gridBagLayout.rowHeights = new int[]{20, 10, 20, 10, 20, 10, 20, 20, 20};

        setLayout(gridBagLayout);

        var sourceColorLabel = new JLabel("Source Color:");
        var gbc_sourceColorLabel = new GridBagConstraints();
        gbc_sourceColorLabel.fill = GridBagConstraints.HORIZONTAL;
        gbc_sourceColorLabel.insets = new Insets(0, 0, 5, 0);
        gbc_sourceColorLabel.gridx = 0;
        gbc_sourceColorLabel.gridy = 0;
        add(sourceColorLabel, gbc_sourceColorLabel);

        sourceColorField = new JTextField();
        textFields.add(sourceColorField);
        var gbc_sourceColorField = new GridBagConstraints();
        gbc_sourceColorField.gridwidth = 2;
        gbc_sourceColorField.fill = GridBagConstraints.HORIZONTAL;
        gbc_sourceColorField.insets = new Insets(0, 0, 5, 0);
        gbc_sourceColorField.gridx = 1;
        gbc_sourceColorField.gridy = 0;
        add(sourceColorField, gbc_sourceColorField);
        sourceColorField.setColumns(10);

        BasicArrowButton sourceColorButton = new BasicArrowButton(BasicArrowButton.EAST);
        sourceColorButton.addActionListener((ae) -> sourceColorPressed());
        GridBagConstraints gbc_sourceColorButton = new GridBagConstraints();
        gbc_sourceColorButton.fill = GridBagConstraints.HORIZONTAL;
        gbc_sourceColorButton.insets = new Insets(0, 0, 5, 5);
        gbc_sourceColorButton.gridx = 4;
        gbc_sourceColorButton.gridy = 0;
        add(sourceColorButton, gbc_sourceColorButton);

        JLabel targetColorLabel = new JLabel("Target Color:");
        GridBagConstraints gbc_targetColorLabel = new GridBagConstraints();
        gbc_targetColorLabel.fill = GridBagConstraints.HORIZONTAL;
        gbc_targetColorLabel.insets = new Insets(5, 0, 5, 0);
        gbc_targetColorLabel.gridx = 0;
        gbc_targetColorLabel.gridy = 2;
        add(targetColorLabel, gbc_targetColorLabel);

        targetColorField = new JTextField();
        textFields.add(targetColorField);
        GridBagConstraints gbc_targetColorField = new GridBagConstraints();
        gbc_targetColorField.gridwidth = 2;
        gbc_targetColorField.fill = GridBagConstraints.HORIZONTAL;
        gbc_targetColorField.insets = new Insets(5, 0, 5, 0);
        gbc_targetColorField.gridx = 1;
        gbc_targetColorField.gridy = 2;
        add(targetColorField, gbc_targetColorField);
        targetColorField.setColumns(10);

        BasicArrowButton targetColorButton = new BasicArrowButton(BasicArrowButton.EAST);
        targetColorButton.addActionListener((ae) -> targetColorPressed());
        GridBagConstraints gbc_targetColorButton = new GridBagConstraints();
        gbc_targetColorButton.fill = GridBagConstraints.HORIZONTAL;
        gbc_targetColorButton.insets = new Insets(5, 0, 5, 5);
        gbc_targetColorButton.gridx = 4;
        gbc_targetColorButton.gridy = 2;
        add(targetColorButton, gbc_targetColorButton);

        JLabel targetDirectoryLabel = new JLabel("Target Directory:");
        targetDirectoryLabel.setHorizontalAlignment(SwingConstants.LEFT);
        GridBagConstraints gbc_targetDirectoryLabel = new GridBagConstraints();
        gbc_targetDirectoryLabel.fill = GridBagConstraints.HORIZONTAL;
        gbc_targetDirectoryLabel.insets = new Insets(5, 0, 5, 0);
        gbc_targetDirectoryLabel.gridx = 0;
        gbc_targetDirectoryLabel.gridy = 4;
        add(targetDirectoryLabel, gbc_targetDirectoryLabel);

        targetDirectoryField = new JTextField();
        textFields.add(targetDirectoryField);
        GridBagConstraints gbc_targetDirectoryField = new GridBagConstraints();
        gbc_targetDirectoryField.gridwidth = 2;
        gbc_targetDirectoryField.fill = GridBagConstraints.HORIZONTAL;
        gbc_targetDirectoryField.insets = new Insets(5, 0, 5, 0);
        gbc_targetDirectoryField.gridx = 1;
        gbc_targetDirectoryField.gridy = 4;
        add(targetDirectoryField, gbc_targetDirectoryField);
        targetDirectoryField.setColumns(10);

        BasicArrowButton targetDirectoryButton = new BasicArrowButton(BasicArrowButton.EAST);
        targetDirectoryButton.addActionListener((ae) -> targetDirectoryPressed());
        GridBagConstraints gbc_targetDirectoryButton = new GridBagConstraints();
        gbc_targetDirectoryButton.fill = GridBagConstraints.HORIZONTAL;
        gbc_targetDirectoryButton.insets = new Insets(5, 0, 10, 5);
        gbc_targetDirectoryButton.gridx = 4;
        gbc_targetDirectoryButton.gridy = 4;
        add(targetDirectoryButton, gbc_targetDirectoryButton);

        JLabel targetFormatLabel = new JLabel("Target Format:");
        targetFormatLabel.setHorizontalAlignment(SwingConstants.LEFT);
        GridBagConstraints gbc_targetFormatLabel = new GridBagConstraints();
        gbc_targetFormatLabel.anchor = GridBagConstraints.WEST;
        gbc_targetFormatLabel.insets = new Insets(5, 0, 10, 0);
        gbc_targetFormatLabel.gridx = 0;
        gbc_targetFormatLabel.gridy = 6;
        add(targetFormatLabel, gbc_targetFormatLabel);

        targetFormatBox = new JComboBox<>(new String[]{"SVG", "EMF"});
        GridBagConstraints gbc_targetFormatBox = new GridBagConstraints();
        gbc_targetFormatBox.gridwidth = 2;
        gbc_targetFormatBox.anchor = GridBagConstraints.WEST;
        gbc_targetFormatBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_targetFormatBox.insets = new Insets(5, 0, 10, 0);
        gbc_targetFormatBox.gridx = 1;
        gbc_targetFormatBox.gridy = 6;
        add(targetFormatBox, gbc_targetFormatBox);

        convertButton = new JButton("Convert");
        convertButton.setEnabled(false);
        convertButton.addActionListener((ae) -> startConvertion());
        var gbc_convertButton = new GridBagConstraints();
        gbc_convertButton.fill = GridBagConstraints.HORIZONTAL;
        gbc_convertButton.gridwidth = 3;
        gbc_convertButton.anchor = GridBagConstraints.EAST;
        gbc_convertButton.insets = new Insets(10, 0, 0, 5);
        gbc_convertButton.gridx = 2;
        gbc_convertButton.gridy = 8;
        add(convertButton, gbc_convertButton);

        Border emptyBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        Border labelBorder = BorderFactory.createTitledBorder("Control");
        Border compoundBorder = BorderFactory.createCompoundBorder(labelBorder, emptyBorder);
        setBorder(compoundBorder);

        for (JTextField textField : textFields) {
            textField.getDocument().addDocumentListener(new InputDocumentListener());
        }
    }

    /**
     * Liefert das MainFrame
     *
     * @return Das MainFrame.
     */
    public MainFrame getMainFrame() {
        return mainFrame;
    }

    /**
     * oeffnet einen Filechooser, in dem man die Target Directory angeben kann.
     */
    private void targetDirectoryPressed() {
        JFileChooser fc = new JFileChooser(new File(System.getProperty("user.home") + System.getProperty("file.separator") + "Desktop"));
        fc.setDialogTitle("Select target directory");
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int choice = fc.showOpenDialog(this);
        try {
            if (choice == JFileChooser.APPROVE_OPTION) {
                String selectedFilePath = fc.getSelectedFile().getAbsolutePath();
                setTargetDirectory(selectedFilePath);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * oeffnet den Colorchooser, wenn man den Knopf bei der Source Color waehlt.
     * Die zurueckgegebene Farbe wird in das Hexardezimale konvertiert und dann im entsprechenden TextField gesetzt.
     */
    private void sourceColorPressed() {
        Color color = JColorChooser.showDialog(this, "Select a color", Color.WHITE);
        if (color != null) {
            setSourceColor(Integer.toHexString(color.getRGB()).substring(2).toUpperCase());
        }
    }

    /**
     * oeffnet den Colorchooser, wenn man den Knopf bei der Target Color waehlt.
     * Die zurueckgegebene Farbe wird in das Hexardezimale konvertiert und dann im entsprechenden TextField gesetzt.
     */
    private void targetColorPressed() {
        Color color = JColorChooser.showDialog(this, "Select a color", Color.WHITE);
        if (color != null) {
            setTargetColor(Integer.toHexString(color.getRGB()).substring(2).toUpperCase());
        }
    }

    /**
     * Startet den Konvertierungsprozess.
     * Zuerst werden die Eingaben kontrolliert.
     * Falls die Erfolgreich war, wird dann ein ProgressbarDialog instanziiert.
     */
    private void startConvertion() {
        try {
            if (!isValidColorHex(getSourceColor())) {
                throw new Exception("Incorrect source color (Capital letters and without \"#\")");
            } else if (!isValidColorHex(getTargetColor())) {
                throw new Exception("Incorrect target color (Capital letters and without \"#\")");
            }
            var file = new File((getTargetDirectory()));
            if (!file.exists()) {
                throw new Exception("Target Directory does not exist");
            }
            new ProgressbarDialog(mainFrame, mainFrame.getControlPanel(), controller);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainFrame, e.getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Liefert den Text im Source Color TextField.
     *
     * @return Der Text im sourceColorField.
     */
    public String getSourceColor() {
        return sourceColorField.getText();
    }

    /**
     * Setzt den Text im Source Color TextField.
     *
     * @param sourceColor Der Text im SourceColorField.
     */
    public void setSourceColor(String sourceColor) {
        sourceColorField.setText(sourceColor);
    }

    /**
     * Liefert den Text im Target Color TextField.
     *
     * @return Der Text im targetColorField.
     */
    public String getTargetColor() {
        return targetColorField.getText();
    }

    /**
     * Setzt den Text im Target Color TextField.
     *
     * @param targetColor Der Text im targetColorField.
     */
    public void setTargetColor(String targetColor) {
        targetColorField.setText(targetColor);
    }

    /**
     * Liefert den Text im Target Directory Field.
     *
     * @return Der Text im targetDirectoryField.
     */
    public String getTargetDirectory() {
        return targetDirectoryField.getText();
    }

    /**
     * Setzt den Text im Target Directory Field.
     *
     * @param targetDirectory Der Text im targetDirectoryField.
     */
    public void setTargetDirectory(String targetDirectory) {
        targetDirectoryField.setText(targetDirectory);
    }

    /**
     * Liefert das momentan gewaehlte Format in der ComboBox.
     *
     * @return Das selektierte Objekt in der targetFormatBox.
     */
    public String getTargetFormat() {
        return targetFormatBox.getSelectedItem().toString().toUpperCase();
    }

    /**
     * Testet, ob der angegebene String sich im fuer das Programm korrekte Hex Format befindet.
     *
     * @param string Der zu testende String.
     * @return Boolesches Ergebnis, ob der String korrekt formatiert ist oder nicht.
     */
    public boolean isValidColorHex(String string) {
        String regex = "([A-F0-9]{6})$";
        return string.matches(regex);
    }

    /**
     * DocumentListener, der bei jedem Event untersucht, ob die Eingabefelder leer sind.
     *
     * @author Simon Weibel
     * @since 2021-05-18
     */
    private class InputDocumentListener implements DocumentListener {

        /**
         * Untersucht ob alle Textfelder Inputs haben, nachdem eine Eingabe getätigt wurde.
         *
         * @param e Das mitgegebene DocumentEvent.
         */
        @Override
        public void insertUpdate(DocumentEvent e) {
            checkInputs();
        }

        /**
         * Untersucht ob alle Textfelder Inputs haben, nachdem eine Eingabe entfernt wurde.
         *
         * @param e Das mitgegebene DocumentEvent.
         */
        @Override
        public void removeUpdate(DocumentEvent e) {
            checkInputs();
        }

        /**
         * Untersucht ob alle Textfelder Inputs haben, nachdem eine Eingabe editiert wurde.
         *
         * @param e Das mitgegebene DocumentEvent.
         */
        @Override
        public void changedUpdate(DocumentEvent e) {

        }
    }

    /**
     * Untersucht, ob alle Textfelder Inputs haben.
     * Falls dies der Fall ist, wird der Convert Knopf aktiviert.
     */
    private void checkInputs() {
        if (!getSourceColor().isEmpty() && !getTargetColor().isEmpty() && !getTargetDirectory().isEmpty()) {
            convertButton.setEnabled(true);
        } else {
            convertButton.setEnabled(false);
        }
    }
}
