package ch.ost.rj.ifs.icon_converter.view;

import ch.ost.rj.ifs.icon_converter.controller.Controller;
import ch.ost.rj.ifs.icon_converter.model.Icon;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.NoSuchElementException;

/**
 * Das Hauptfenster der Benutzeroberflaeche.
 * In ihm werden zwei Panels (IconsPanel und ControlPanel) zusammengefuehrt, so wie auch eine Menueleiste hizugefuegt.
 *
 * @author Simon Weibel
 * @since 2021-05-12
 */
public class MainFrame extends JFrame {

    private JMenuItem openItem;
    private IconsPanel iconsPanel;
    private ControlPanel controlPanel;

    private Controller controller;

    /**
     * Konstrukter vom Hauptfenster.
     * Er setzt die Komponenten zusammen und konfiguriert das Frame.
     *
     * @param controller Controller
     */
    public MainFrame(Controller controller) {
        this.controller = controller;

        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        var panelContainer = new JPanel();
        contentPane.add(panelContainer, BorderLayout.CENTER);

        var gridBagLayout = new GridBagLayout();
        panelContainer.setLayout(gridBagLayout);

        var gbc_iconsPanel = new GridBagConstraints();
        gbc_iconsPanel.gridx = 0;
        gbc_iconsPanel.gridy = 0;
        gbc_iconsPanel.fill = GridBagConstraints.BOTH;
        gbc_iconsPanel.weightx = 1;
        gbc_iconsPanel.weighty = 1;
        gbc_iconsPanel.insets = new Insets(10, 10, 5, 10);
        panelContainer.add(createIconsPanel(), gbc_iconsPanel);

        var gbc_controlPanel = new GridBagConstraints();
        gbc_controlPanel.gridx = 1;
        gbc_controlPanel.gridy = 0;
        gbc_controlPanel.fill = GridBagConstraints.BOTH;
        gbc_controlPanel.insets = new Insets(10, 5, 10, 10);
        panelContainer.add(createControlPanel(), gbc_controlPanel);

        setTitle("Icon Converter");
        setJMenuBar(createMenuBar());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            ImageIcon IMAGE_ICON = new ImageIcon(getClass().getResource("/images/logo.png"));
            setIconImage(IMAGE_ICON.getImage());
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

        pack();
        setLocation(new Point((Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2));
        setResizable(false);
        setVisible(true);
    }

    /**
     * Erstellt ein JMenuBar mit der Option "Open..."
     *
     * @return Eine Menueleiste mit einem Reiter "File", in dem sich die Option "Open..." befindet.
     */
    public JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        openItem = new JMenuItem("Open...");
        openItem.addActionListener((ae) -> openPressed());

        fileMenu.add(openItem);
        menuBar.add(fileMenu);
        return menuBar;
    }

    /**
     * Weist der Variable "iconsPanel" das Objekt IconsPanel zu und gibt diese dann zurueck.
     *
     * @return Ein IconsPanel.
     */
    public JPanel createIconsPanel() {
        iconsPanel = new IconsPanel(this, controller);
        return iconsPanel;
    }

    /**
     * Weist der Variable "controlPanel" das Objekt ControlPanel zu und gibt diese dann zurueck.
     *
     * @return Ein ControlPanel.
     */
    public JPanel createControlPanel() {
        controlPanel = new ControlPanel(this, controller);
        return controlPanel;
    }

    /**
     * Methode, welche einen JFileChooser oeffnet, welcher Ordner mit SVG(s) oder SVG(s) selber waehlt.
     * Diese werden dann im Model abgespeichert ueber den Controller.
     */
    private void openPressed() {
        iconsPanel.getSelectableIconsList().clearSelection();
        JFileChooser fc = new JFileChooser(new File(System.getProperty("user.home") + System.getProperty("file.separator") + "Desktop"));
        fc.setMultiSelectionEnabled(true);
        fc.setDialogTitle("Select one or more Icons or folders containing Icons");
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int choice = fc.showOpenDialog(this);

        try {
            if (choice == JFileChooser.APPROVE_OPTION) {
                File[] selectedFiles = fc.getSelectedFiles();

                boolean addedFile = false;
                for (File f : selectedFiles) {
                    if (f.toString().endsWith(".svg")) {
                        controller.addIcon(new ch.ost.rj.ifs.icon_converter.model.Icon(f.toPath()));
                        addedFile = true;
                    } else if (f.isDirectory()) {
                        for (File f2 : f.listFiles()) {
                            if (f2.toString().endsWith(".svg")) {
                                controller.addIcon(new Icon(f2.toPath()));
                                addedFile = true;
                            }
                        }
                    }
                }
                if (!addedFile) {
                    if (selectedFiles.length > 1) {
                        throw new NoSuchElementException("No Icons found in selected Files");
                    } else {
                        throw new NoSuchElementException("No Icons found in selected File");
                    }
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Warnnig", JOptionPane.WARNING_MESSAGE);
            openPressed();
        }
    }

    /**
     * Liefert das ControlPanel.
     *
     * @return Das controlPanel.
     */
    public ControlPanel getControlPanel() {
        return controlPanel;
    }
}
