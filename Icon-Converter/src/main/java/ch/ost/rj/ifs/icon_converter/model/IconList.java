package ch.ost.rj.ifs.icon_converter.model;

import java.util.ArrayList;

/**
 * Repraesentiert eine Liste von Icon Objekten.
 *
 * @author Simon Weibl
 * @since 2021-05-12
 */
public class IconList {

    private ArrayList<Icon> iconList = new ArrayList<>();

    /**
     * Leerer Konstruktor der Klasse IconList
     */
    public IconList() {
    }

    /**
     * get Methode fuer die IconList.
     * Liefert die Icons.
     *
     * @return iconList Die IconListe
     */
    public ArrayList<Icon> getIcons() {
        return iconList;
    }

    /**
     * Fuegt ein Icon der Liste hinzu.
     *
     * @param icon Das zu hinzufuegende Icon.
     */
    public void addIcon(Icon icon) {
        if (!iconList.contains(icon)) {
            iconList.add(icon);
        }
    }

    /**
     * Liefert den Index des Icons in der Liste.
     *
     * @param icon Das Icon, wessen Index man erhalten moechte.
     * @return Der Index des Icons.
     */
    public int indexOf(Icon icon) {
        return iconList.indexOf(icon);
    }

    /**
     * Liefert die aktuelle Groesse der Liste.
     *
     * @return Anzahl Elemente in der Liste.
     */
    public int getSize() {
        return iconList.size();
    }
}
