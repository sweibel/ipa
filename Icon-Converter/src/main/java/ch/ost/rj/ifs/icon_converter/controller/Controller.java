package ch.ost.rj.ifs.icon_converter.controller;

import ch.ost.rj.ifs.icon_converter.model.Icon;
import ch.ost.rj.ifs.icon_converter.model.IconList;
import ch.ost.rj.ifs.icon_converter.view.ProgressbarDialog;

import java.util.ArrayList;

/**
 * Bildet die Schnittstelle zwischen Model und View.
 * In Ihr werden die Listener gemeldet und auch ausgefuehrt.
 * Zudem speichert Sie hinzugefuegte Icons im Model ab.
 *
 * @author Simon Weibel
 * @since 2021-05-12
 */
public class Controller {

    private IconList iconList = new IconList();
    private ArrayList<ListListener> listListeners = new ArrayList<>();
    private ArrayList<ConverterListener> converterListeners = new ArrayList<>();
    private ArrayList<ProgressbarListener> progressbarListeners = new ArrayList<>();

    /**
     * Leerer Konstruktor der Klasse Controller.
     */
    public Controller() {

    }

    /**
     * Fuegt Icon der Liste hinzu und loest auch gleich den Listener auf, der die IconListe im IconsPanel benachrichtigt, so dass die Liste aktualisiert werden kann.
     *
     * @param icon Das zu hinzufuegende Icon.
     */
    public void addIcon(Icon icon) {
        iconList.addIcon(icon);
        listListeners.stream().forEach((l) -> l.iconAdded(icon));
    }

    /**
     * Startet den WorkerThread.
     *
     * @param progressbarDialog Die Fortschrittsanzeige.
     */
    public void startConverterThread(ProgressbarDialog progressbarDialog) {
        var converterThread = new ConverterThread(progressbarDialog, this);
        converterThread.start();
    }

    /**
     * Benachrichtigt alle ConverterListeners, dass der Konvertierungsprozess abgebrochen wurde.
     */
    public void abortConversion() {
        converterListeners.stream().forEach((l) -> l.abortPressed());
    }

    /**
     * Benachrichtigt alle ProgressbarListeners, dass der Konvertierungsprozess erfolgreich abgeschlossen wurde.
     */
    public void finishedConversion() {
        progressbarListeners.stream().forEach((l) -> l.convertingFinished());

    }

    /**
     * Benachrichtigt alle ProgressbarListeners, dass der Konvertierungsprozess fehgeschlagen ist.
     */
    public void failedConversion() {
        progressbarListeners.stream().forEach((l) -> l.convertingFailed());
    }

    /**
     * Liefert die IconListe.
     *
     * @return Die zu liefernde IconListe.
     */
    public IconList getIconList() {
        return iconList;
    }

    /**
     * Fuegt einen ListListener hinzu.
     *
     * @param listener Der zu hinzufuegende Listener.
     */
    public void addListListener(ListListener listener) {
        listListeners.add(listener);
    }

    /**
     * Fuegt einen ConverterListener hinzu.
     *
     * @param listener Der zu hinzufuegende Listener.
     */
    public void addConverterListener(ConverterListener listener) {
        converterListeners.add(listener);
    }

    /**
     * Fuegt einen ProgressbarListener hinzu.
     *
     * @param listener Der zu hinzufuegende Listener.
     */
    public void addProgressbarListener(ProgressbarListener listener) {
        progressbarListeners.add(listener);
    }
}
