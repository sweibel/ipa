package ch.ost.rj.ifs.icon_converter.controller;

import ch.ost.rj.ifs.icon_converter.model.Icon;

/**
 * Listener, welcher auf das Hinzufuegen von Icons reagiert.
 *
 * @author Simon Weibel
 * @since 2021-05-14
 */
public interface ListListener {

    /**
     * Methode, welche aufgerufen wird, wenn ein Icon hinzugefuegt wurde.
     *
     * @param icon Das hinzugefuegte Icon.
     */
    void iconAdded(Icon icon);

}
